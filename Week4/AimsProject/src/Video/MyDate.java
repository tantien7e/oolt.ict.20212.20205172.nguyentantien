package Video;

import java.time.LocalDate;
import java.util.Scanner;

public class MyDate {
    int day;
    int month;
    int year;
    String dayStr, monthStr, yearStr;
    static int monthArr[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    static String monthName[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};

    public MyDate(){ //The current date
        LocalDate today = LocalDate.now();
        this.day = today.getDayOfMonth();
        this.month = today.getMonthValue();
        this.year = today.getYear();
    }
    public void printDate(){ //Print Date without returning anything according to this format: “February 29th 2020”
        System.out.println("Date: "+ monthName[month-1] + " " + day + "th "+ year);
    }
    public String printDate2(){ //Print Date with returned String
        return "Date: "+ monthName[month-1] + " " + day + "th "+ year;
    }
    public MyDate(int day, int month, int year) { //Construct with full date in numbers
        setDay(day);
        setMonth(month);
        setYear(year);
    }

    public MyDate(String day, String month, String year){ // Construct with 3 String input
        setDayStr(day);
        setMonthStr(month);
        setYearStr(year);
    }
    public MyDate(String date) {
        this.accept(date);
    } // Construct with 1 String input

    public void resetDate(int day, int month, int year){
        this.setYear(year);
        this.setMonth(month);
        this.setDay(day);
    }


    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDayStr() {
        return dayStr;
    }

    public String getMonthStr() {
        return monthStr;
    }

    public String getYearStr() {
        return yearStr;
    }

    public void setDayStr(String dayStr) {
        this.dayStr = dayStr;
    }

    public void setMonthStr(String monthStr) {
        this.monthStr = monthStr;
    }

    public void setYearStr(String yearStr) {
        this.yearStr = yearStr;
    }

    public void accept() {
        System.out.print("Enter date (eg: \"March 03 2002\"): ");
        Scanner scanner = new Scanner(System.in);
        String date = scanner.nextLine();
        accept(date);
    }
    public void accept(String date) {
        int marker = 0;
        String space = " ";
        marker = date.indexOf(space);
        String monthInput = date.substring(0, marker);
        for(int i = 0; i<12; i++){
            if(monthInput.equals(this.monthName[i])) setMonth(monthArr[i]);
        }
        int dayInput = Integer.parseInt(date.substring(marker+1, marker+3));
        setDay(dayInput);
        int yearInput = Integer.parseInt(date.substring(marker+4));
        setYear(yearInput);
    }
    public boolean compareYear(MyDate date){
        return this.year< date.year;
    }
    public boolean compareMonth(MyDate date){
        return this.month< date.month;
    }
    public boolean compareDay(MyDate date){
        return this.day< date.day;
    }

}