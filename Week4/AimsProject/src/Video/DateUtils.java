package Video;

public class DateUtils {
    public static int compareDate(MyDate date1, MyDate date2){ //return 1 if date1 before date 2, -1 if date1 after, and 0 if 2 dates are the same
        if(date1.year < date2.year) return 1;
        if(date1.year > date2.year) return -1;
        else {
            if(date1.month <date2.month) return 1;
            if(date1.month >date2.month) return -1;
            else{
                if(date1.day < date2.day) return 1;
                if(date1.day > date2.day) return -1;
                else return 0;
            }
        }
    }
    public static void printCompareDate(MyDate date1, MyDate date2){ // print the 2 compare dates
        int key = compareDate(date1, date2);
        switch (key){
            case 1:
                System.out.println(date1.printDate2() + " occurs before " + date2.printDate2());
                break;
            case -1:
                System.out.println(date1.printDate2() + " occurs after " + date2.printDate2());
                break;
            case 0:
                System.out.println("The two dates are the same");
                break;
        }
    }
    public static void swapDate(MyDate date1, MyDate date2){ // swap 2 dates
        MyDate temp = new MyDate();
        temp.resetDate(date1.getDay(), date1.getMonth(), date1.getYear());
        date1.resetDate(date2.getDay(), date2.getMonth(), date2.getYear());
        date2.resetDate(temp.getDay(), temp.getMonth(), temp.getYear());
    }
    public static void sortDate(MyDate[] dateArr){ //Sorting an array of Date using Selection Sort
        for(int i = 0; i<dateArr.length; i++){
            int key = i;
            for(int j = i+1; j<dateArr.length; j++){
                if(DateUtils.compareDate(dateArr[j], dateArr[key]) == 1) key = j;
            }
            DateUtils.swapDate(dateArr[i], dateArr[key]);
        }
        System.out.println("The Dates after being sorted: ");
        printDateArr(dateArr);
    }
    public static void printDateArr( MyDate[] dateArr){ // print an array of Dates
        for(int i = 0; i<dateArr.length; i++){
            System.out.print(i+1 + ". ");
            dateArr[i].printDate();
        }
    }
}
