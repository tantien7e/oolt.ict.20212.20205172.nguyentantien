package Video;

public class DigitalVideoDisc {
    private String title;
    private String category;
    private String director;
    private int length;
    private float cost;

    public DigitalVideoDisc(String title){
        this.title = title;
    }
    public DigitalVideoDisc(String title, String category){
        this(title);
        this.category = category;
    }
    public DigitalVideoDisc(String title, String category, String director){
        this(title, category);
        this.director = director;
    }
    public DigitalVideoDisc(String title, String category, String director, int length, int cost){
        this(title, category, director);
        this.length = length;
        this.cost = cost;
    }

    public void equals(DigitalVideoDisc copy){
        setTitle(copy.getTitle());
        setCategory(copy.getCategory());
        setDirector(copy.getDirector());
        setLength(copy.getLength());
        setCost(copy.getCost());
    }

    public void printDVD(){
        System.out.println("DVD: " + this.getTitle()+ " - "+this.getCategory()+ " - "+ this.getDirector()+ " - "+ this.getLength()+ ": $"+ this.getCost());
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public String getDirector() {
        return director;
    }

    public int getLength() {
        return length;
    }

    public float getCost() {
        return cost;
    }
}
