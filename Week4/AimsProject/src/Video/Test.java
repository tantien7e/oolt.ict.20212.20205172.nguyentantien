package Video;

public class Test {


    public static void main(String[] args) {
        MyDate date1 = new MyDate("March 03 2002");
        MyDate date2 = new MyDate(1, 1, 2001);
        MyDate date3 = new MyDate(26, 3, 1998);
        MyDate date4 = new MyDate(26, 3, 2030);
        MyDate date5 = new MyDate(9, 7, 2016);
        MyDate[] dateArr = {date1, date2, date3, date4, date5};

        DateUtils.printDateArr(dateArr);
        DateUtils.sortDate(dateArr);


    }
}

