package Video;

public class DateUtils {
    public static int compareDate(MyDate date1, MyDate date2){
        if(date1.year < date2.year) return 1;
        if(date1.year > date2.year) return -1;
        else {
            if(date1.month <date2.month) return 1;
            if(date1.month >date2.month) return -1;
            else{
                if(date1.day < date2.day) return 1;
                if(date1.day > date2.day) return -1;
                else return 0;
            }
        }
    }

}
