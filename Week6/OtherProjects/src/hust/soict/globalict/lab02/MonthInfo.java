package hust.soict.globalict.lab02;

import java.util.InputMismatchException;
import java.util.Scanner;
class Date {
    int year;
    String month;
    int imonth;
    String[][] validMonthInput = {
            {"January", "Jan.", "Jan", "1", "31"},
            {"February", "Feb.", "Feb", "2", "28"},
            {"March", "Mar.", "Mar", "3", "31"},
            {"April", "Apr.", "Apr", "4", "30"},
            {"May", "May.", "May", "5", "31"},
            {"June", "Jun.", "Jun", "6", "30"},
            {"July", "Jul.", "Jul", "7", "31"},
            {"August", "Aug.", "Aug", "8", "31"},
            {"September", "Sep.", "Sep", "9", "30"},
            {"October", "Oct.", "Oct", "10", "31"},
            {"November", "Nov.", "Nov", "11", "31"},
            {"December", "Dec.", "Dec", "12", "31"}
    };
    Date(int year, String month){
        this.year = year;
        this.month = month;
        if(this.isLeap()){
            validMonthInput[1][4] ="29";
            this.isMonthValid();
        }
    }
    boolean isLeap() {
        if(this.year % 4 == 0 && this.year%100 != 0){
            return true;
        }
        else return this.year % 400 == 0;
    }
    boolean isMonthValid() {
        for(int i = 0; i< validMonthInput.length; i++){
            for(int j =0; j<validMonthInput[i].length; j++){
                if(this.month.equals(validMonthInput[i][j])) {
                    imonth = i+1;
                    return true;
                }
            }
        }
        return false;
    }
    void changeMonth(String month){
        this.month = month;
    }
    int printMonth(){
        return Integer.parseInt(validMonthInput[imonth-1][4]);
    }
}

public class MonthInfo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int year = 0;
        String month;
        boolean legal = false;
        while(!legal){
            try{
                System.out.println("Enter the Year: ");
                year = scanner.nextInt();
                legal = true; 
            } catch(InputMismatchException e){
                System.out.println("Illegal year!");
            }
            scanner.nextLine();
        }
        System.out.println("Enter the month: ");
        month = scanner.next();
        Date date = new Date(year, month);
        while(!date.isMonthValid()){
            System.out.println("Please re-enter the month: ");
            String newMonth = scanner.next();
            date.changeMonth(newMonth);
        }
        System.out.print("Succeeded! The month "+date.imonth + " has "+ date.printMonth() + " days");
    }
}
