package hust.soict.globalict.aims;

import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Aims {
    static Order[] newOrder = new Order[5];
    static ArrayList<Media> mediaArr = new ArrayList<>();
    public static void main(String[] args) {

        //DVD 1
        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
        dvd1.setCategory("Animation");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLength(87);
        dvd1.setId(1);

        //DVD 2
        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star wars");
        dvd2.setCategory("Science fiction");
        dvd2.setCost(24.95f);
        dvd2.setDirector("George Lucas");
        dvd2.setLength(124);
        dvd2.setId(2);

        //DVD3
        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
        dvd3.setCategory("Animation");
        dvd3.setCost(18.99f);
        dvd3.setDirector("John Musker");
        dvd3.setLength(87);
        dvd3.setId(3);

        //Book 1
        Book book1 = new Book("O long Vien", "Comic", new ArrayList<String>(Arrays.asList("james", "Dang")));
        book1.setId(4);

        //Add content to the Media Array
        mediaArr.add(dvd1);
        mediaArr.add(dvd2);
        mediaArr.add(dvd3);
        mediaArr.add(book1);
        Scanner scanner = new Scanner(System.in);
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.print("Please choose a number: 0-1-2-3-4: ");
        showMenu(scanner);
        scanner.close();
    }

    public static void showMenu(Scanner scanner) {
        int function = scanner.nextInt();

        switch (function){
            case 1:
                if(Order.nbOrders<Order.MAX_LIMITED_ORDER){
                    newOrder[Order.nbOrders] = Order.createOrder();
                }
                else{
                    System.out.println("Maximum number reached!");
                }
                break;
            case 2:
                boolean foundAdd = false;
                System.out.print("Enter the id of the Order: ");
                int idOrder = scanner.nextInt();
                System.out.print("Enter the id of the Media: ");
                int idMedia = scanner.nextInt();
                for(Media x: mediaArr){
                    if(x.getId() == idMedia){
                        foundAdd = true;
                        newOrder[idOrder].addMedia(x);
                    }
                }
                if(!foundAdd) {
                    System.out.println("Cannot find the media!");
                }
                break;
            case 3:
                boolean foundRemv = false;
                System.out.print("Enter the id of the Order: ");
                idOrder = scanner.nextInt();
                System.out.print("Enter the id of the Media: ");
                idMedia = scanner.nextInt();
                for(Media x: mediaArr){
                    if(x.getId() == idMedia){
                        foundRemv = true;
                        newOrder[idOrder].removeMedia(x);
                    }
                }
                if(!foundRemv){
                    System.out.println("Cannot find the media!");
                }
                break;
            case 4:
                System.out.print("Enter the id of the Order: ");
                idOrder = scanner.nextInt();
                newOrder[idOrder].printOrder();
                break;
        }
        if(function != 0) {
            showMenu(scanner);
        }
    }
}

