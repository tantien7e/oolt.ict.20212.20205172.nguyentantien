package hust.soict.globalict.aims.media;

import java.util.Scanner;

public class DigitalVideoDisc extends Media{
    private String director;
    private int length;
    private float cost;

    public DigitalVideoDisc(String title){
        super(title);
    }
    public DigitalVideoDisc(String title, String category){
        super(title, category);
    }
    public DigitalVideoDisc(String title, String category, String director){
        super(title, category);
        this.director = director;
    }
    public DigitalVideoDisc(String title, String category, String director, int length, int cost){
        this(title, category, director);
        this.length = length;
        this.cost = cost;
    }

    public void equals(DigitalVideoDisc copy){ // copy an DVD to another DVD
        setTitle(copy.getTitle());
        setCategory(copy.getCategory());
        setDirector(copy.getDirector());
        setLength(copy.getLength());
        setCost(copy.getCost());
    }

    public void printDVD(){
        System.out.println("DVD: " + this.getTitle()+ " - "+this.getCategory()+ " - "+ this.getDirector()+ " - "+ this.getLength()+ ": $"+ this.getCost());
    }

    public boolean search(String title){
        boolean found = true;
        String titleTrimed = title.trim().replaceAll(" +", " ").toLowerCase(); //delete all redundant white spaces
        Scanner scan = new Scanner(titleTrimed);
        scan.useDelimiter(" ");
        scan.tokens();
        while(scan.hasNext()){
            if(!this.getTitle().toLowerCase().contains(scan.next())) found = false;
        }
        return found;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getDirector() {
        return director;
    }

    public int getLength() {
        return length;
    }


}
