package hust.soict.globalict.aims.media;

import java.lang.Object;
import java.util.ArrayList;
import java.util.List;

public class Book extends Media{
    private String title;
    private String category;
    private float cost;
    private List<String> authors = new ArrayList<String>();

    public Book(String title){
        super(title);
    }
    public Book(String title, String category){
        super(title, category);
    }
    public Book(String title, String category, List<String> authors){
        super(title, category);
        this.authors = authors;
    }

    public void addAuthor(String authorName){
        if(this.authors.contains(authorName)){
            System.out.println("This author has already existed!");
        }
        else {
            authors.add(authorName);
            System.out.println("Add new author successfully!");
        }
    }

    public void removeAuthor(String authorName){
        if(this.authors.contains(authorName)){
            authors.remove(authorName);
            System.out.println("Remove successfully!");
        }
        else {
            System.out.println("The author is not in the List of author! Cannot remove");
        }
    }


}
