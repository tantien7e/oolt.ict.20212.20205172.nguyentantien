package hust.soict.globalict.aims.order;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate.MyDate;

import java.util.ArrayList;

public class Order {
    public static final int MAX_NUMBERS_ORDERED = 10;
    public static final int MAX_LIMITED_ORDER = 5;
    public static int nbOrders = 0;
    private int lucky = -1;
    private int id;
    private ArrayList<Media> itemsOrdered = new ArrayList<>();

    public static Order createOrder() { //check whether the order is below the limit number, if higher then return null
        if(nbOrders < MAX_LIMITED_ORDER) {
            System.out.println("Order "+ nbOrders + " has been created successfully!");
            return new Order();
        }
        else {
            System.out.println("Maximum number of orders reached!");
            return null;
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Order(){
        this.id = nbOrders++;
    }

    public void addMedia(Media newMedia){
        if (itemsOrdered.contains(newMedia)) {
            System.out.println("This media has been selected!");
        }
        else{
            this.itemsOrdered.add(newMedia);
            System.out.println("Media added successfully!");
        }

    }
    public void removeMedia(Media deletedMedia){
        if(itemsOrdered.contains(deletedMedia)){
            itemsOrdered.remove(deletedMedia);
            System.out.println("Delete Successfully!");
        }
        else {
            System.out.println("The object does not exist in the order!");
        }
    }

    public float totalCost(){
        float sum = 0;
        for(int i = 0; i<itemsOrdered.size(); i++){
            if(i!= lucky){
                sum+=itemsOrdered.get(i).getCost();
            }
        }
        return sum;
    }

    public Media getALuckyItem(Order order){
        this.lucky = (int) Math.round(Math.random()*(itemsOrdered.size()+1));
        return order.itemsOrdered.get(lucky);
    }
    public void printOrder(){
        MyDate date = new MyDate();
        System.out.println("**************************Order****************************");
        date.printDate();
        System.out.println("Ordered Items: ");
        for(int i = 0; i<itemsOrdered.size(); i++){
            System.out.print(i+1+": ");
            System.out.print(itemsOrdered.get(i).getTitle());
            System.out.println();
        }
        if(this.lucky !=-1) System.out.println("You have a lucky DVD: "+ itemsOrdered.get(lucky).getTitle());
        System.out.println("Total cost: $"+ this.totalCost());
        System.out.println("***********************************************************");
    }
}
