import java.util.Arrays;
class QuickSort{
    int[] intArr;
    int low, high;
    int pi =0;

    QuickSort(int[] arr){
        this.intArr = arr;
        low = 0;
        high = intArr.length-1;
    }

    void processQuickSort(){
        quickSort(intArr, low, high);
    }

    void quickSort(int[] arr, int low, int high){
        if(low<high){
            pi = partition(arr, low, high);
            quickSort(arr, low, pi-1);
            quickSort(arr, pi+1, high);
        }
    }

    int partition(int[] arr, int low, int high){
        int pivot = arr[high];
        int i=low-1;
        for(int j = low; j<high;j++){
            if(arr[j] < pivot){
                i++;
                swap(arr, i, j);
            }
        }
        swap(arr, i+1, high);
        return (i+1);
    }
    void swap(int[] arr, int i, int j){
        int temp = arr[j];
        arr[j] = arr[i];
        arr[i] = temp;
    }
    double calAvr(){
        double sum = 0;
        for(int k:intArr) sum+=k;
        return (sum/intArr.length);
    }
}
public class SortingDemo {
    public static void main(String[] args) {
        int[] k = {1,9,7,5,3,8,6,4};
        QuickSort sort = new QuickSort(k);
        sort.processQuickSort();
        System.out.println(Arrays.toString(sort.intArr));
        System.out.println("The average is: "+ sort.calAvr());
    }
}