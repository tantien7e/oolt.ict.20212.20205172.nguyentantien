import java.lang.Math;
import javax.swing.JOptionPane;
class Equation{
    double[] arrAnsw; 

    void solve(int a, int b){
        arrAnsw= new double[1];
        arrAnsw[0] = -b/a;

    }
    void solve(int a11, int a12, int a21, int a22, int b1, int b2){
        double d = a11*a22 - a21*a12;
        double d1 = b1*a22-b2*a12;
        double d2 = a11*b2-a21*b1;
        if(d != 0){
            arrAnsw = new double[2];
            arrAnsw[0] = d1/d;
            arrAnsw[1] = d2/d;
        }
        else arrAnsw = new double[0];
        
    }
    void solve(int a, int b, int c){
        int delta = b*b-4*a*c;
        if(delta == 0){
            arrAnsw = new double[1];
            arrAnsw[0] = -b/(2*a);
        }
        else if(delta >0){
            arrAnsw = new double[2];
            arrAnsw[0] = (-b+Math.sqrt(delta))/(2*a);
            arrAnsw[1] = (-b-Math.sqrt(delta))/(2*a);
        }
        else {
            arrAnsw = new double[0];
        }
    }
}

public class SolvingEquation {
    public static void main(String[] args) {
        Equation equ = new Equation();
        String type;
        type = JOptionPane.showInputDialog("Choose the type of euqation you want to solve:\n1. Linear equation\n2. A system of first degree equation\n3. Second degree equation with one variable");
        switch(type) {
            case "1": {
                String a, b;
                double x;
                a = JOptionPane.showInputDialog("Enter a:");
                b = JOptionPane.showInputDialog("Enter b:");
                equ.solve(Integer.parseInt(a), Integer.parseInt(b));
                x = equ.arrAnsw[0];
                JOptionPane.showMessageDialog(null, "The result of x is: "+ x);
                break;
            }
            case "2": {
                String a11, a12, b1, b2, a21, a22;
                double x1, x2;
                a11 = JOptionPane.showInputDialog("Enter a11: ");
                a12 = JOptionPane.showInputDialog("Enter a12: ");
                a21 = JOptionPane.showInputDialog("Enter a21:");
                a22 = JOptionPane.showInputDialog("Enter a22:");
                b1 = JOptionPane.showInputDialog("Enter b1:");
                b2 = JOptionPane.showInputDialog("Enter b2:");
                equ.solve(Integer.parseInt(a11),Integer.parseInt(a12),Integer.parseInt(a21),Integer.parseInt(a22),Integer.parseInt(b1),Integer.parseInt(b2));
                switch(equ.arrAnsw.length){
                    case 0: 
                        JOptionPane.showMessageDialog(null, "The system has infinitely many solutions");
                        break;
                    case 2:
                        x1 = equ.arrAnsw[0];
                        x2 = equ.arrAnsw[1];
                        JOptionPane.showMessageDialog(null, "The result of x1 and x2 are: "+ x1 + " and " +x2);
                        break; 
                }
            }
                break;
            case "3":{
                String a,b,c;
                double x1, x2;
                a = JOptionPane.showInputDialog("Enter a:");
                b = JOptionPane.showInputDialog("Enter b:");
                c = JOptionPane.showInputDialog("Enter c:");
                equ.solve(Integer.parseInt(a),Integer.parseInt(b),Integer.parseInt(c));
                switch(equ.arrAnsw.length){
                    case 1: 
                        x1 = equ.arrAnsw[0];
                        JOptionPane.showMessageDialog(null, "The equation has 1 double root x = "+ x1);
                        break;
                    case 2:
                        x1 = equ.arrAnsw[0];
                        x2 = equ.arrAnsw[1];
                        JOptionPane.showMessageDialog(null, "The equation has two distinct roots\n"+"x1 = "+ x1 + "\n"+ "x2 = "+x2);
                        break;
                    case 0: 
                        JOptionPane.showMessageDialog(null, "The equation has no solution!");
                        break;
                }
                break;
            }
        }   
    }
}
