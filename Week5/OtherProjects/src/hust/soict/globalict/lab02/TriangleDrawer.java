package hust.soict.globalict.lab02;

import java.sql.SQLOutput;
import java.util.Scanner;
public class TriangleDrawer {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the number of row you want to draw: ");
        int row = scanner.nextInt();
        int column = 2*row-1;
        for(int i = 1; i<= row; i++){
            for(int j = 1; j<= column; j++){
                if(j>=row-i+1 && j<= row+i-1){
                    System.out.print("*");
                }
                else System.out.print(" ");

            }
            System.out.println();
        }
    }
}
