package hust.soict.globalict.lab02;

class Matrix{
    int row, column;
    int[][] matrix;
    Matrix(int row, int column){
        this.row=row;
        this.column=row;
        matrix = new int[row][column];
    }
    int[][] add(int[][] ob){
        int[][] result = new int[row][column];
        for(int i = 0; i<ob.length; i++){
            for(int j = 0; j<ob[i].length; j++){
                result[i][j] = this.matrix[i][j]+ ob[i][j];
            }
        }
        return result;
    }
}

public class AddMatrix {
    public static void main(String[] args) {
    int[][] a = {{1,2}, {3,4}};
    int[][] b = {{1,6}, {5,4}};
    int[][] result = new int[2][2];
    Matrix myMtrx = new Matrix(2,2);
    myMtrx.matrix = a;
    result = myMtrx.add(b);
    for(int i = 0; i< result.length;i++){
        System.out.print("[ ");
        for(int j = 0; j<result[i].length;j++){
            System.out.print(+ result[i][j]+ " ");
        }
        System.out.print("]");
        System.out.println();
    }

    }
}
