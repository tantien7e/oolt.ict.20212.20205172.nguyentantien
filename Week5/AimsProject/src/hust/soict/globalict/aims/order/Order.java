package hust.soict.globalict.aims.order;

import hust.soict.globalict.aims.disc.DigitalVideoDisc;
import hust.soict.globalict.aims.utils.MyDate.MyDate;

public class Order {
    public static final int MAX_NUMBERS_ORDERED = 10;
    int qtyOrdered =-1;
    private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
    public static final int MAX_LIMITED_ORDER = 5;
    public static int nbOrders = 0;
    private int lucky = -1;

    public static Order createOrder() { //check whether the order is below the limit number, if higher then return null
        if(nbOrders < MAX_LIMITED_ORDER) return new Order();
        else {
            System.out.println("Maximum number of orders reached!");
            return null;
        }
    }
    public Order(){
        if(nbOrders < MAX_LIMITED_ORDER) {
            nbOrders++;
        }

    }
    public void addDigitalVideoDisc(DigitalVideoDisc disc){
        if(qtyOrdered<MAX_NUMBERS_ORDERED){
            qtyOrdered++;
            itemsOrdered[qtyOrdered] = disc;
            System.out.println("The disc has been added successfully!");
        }
        else{
            System.out.println("The maximum number of items is reached!");
        }

    }
    public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList){ // Add an array of disc to an Order
        if(qtyOrdered+dvdList.length <MAX_NUMBERS_ORDERED){
            for(int i = 0; i<dvdList.length; i++){
                itemsOrdered[++qtyOrdered] = dvdList[i];
            }
            System.out.println("The " +dvdList.length + " discs has been added successfully!");
        }
        else{
            System.out.println("The maximum number is reached!");
        }
    }

    public void addDigitalVideoDisc(DigitalVideoDisc dvd1, DigitalVideoDisc dvd2){ // Add two disc to an Order
            this.addDigitalVideoDisc(dvd1);
            this.addDigitalVideoDisc(dvd2);
    }


    public void removeDigitalVideoDisc(DigitalVideoDisc disc){
        boolean found = false;
        for(int i = 0; i<=qtyOrdered; i++){
            found = true;
            if(disc.getTitle().equals(itemsOrdered[i].getTitle())){
                for(int j = i; j<qtyOrdered; j++){
                    itemsOrdered[j].equals(itemsOrdered[j+1]);
                }
                qtyOrdered--;
                System.out.println("Remove succeeded!");
            }
        }
        if(!found) System.out.println("This item is not on your list!");
    }
    public float totalCost(){
        float sum = 0;
        for(int i = 0; i<=qtyOrdered; i++){
            if(i!= lucky){
                sum+=itemsOrdered[i].getCost();
            }
        }
        return sum;
    }

    public DigitalVideoDisc getALuckyItem(Order order){
        this.lucky = (int) Math.round(Math.random()*(qtyOrdered+1));
        return order.itemsOrdered[lucky];
    }
    public void printOrder(){
        MyDate date = new MyDate();
        System.out.println("**************************Order****************************");
        date.printDate();
        System.out.println("Ordered Items: ");
        for(int i = 0; i<=qtyOrdered; i++){
            itemsOrdered[i].printDVD();
        }
        if(this.lucky !=-1) System.out.println("You have a lucky DVD: "+ itemsOrdered[lucky].getTitle());
        System.out.println("Total cost: $"+ this.totalCost());
        System.out.println("***********************************************************");
    }
}
