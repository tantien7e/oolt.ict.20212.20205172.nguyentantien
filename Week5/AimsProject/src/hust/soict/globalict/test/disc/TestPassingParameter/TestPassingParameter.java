package hust.soict.globalict.test.disc.TestPassingParameter;

import hust.soict.globalict.aims.disc.DigitalVideoDisc;

public class TestPassingParameter {
    public static void main(String[] args) {
        DigitalVideoDisc jungleDVD = new DigitalVideoDisc("Jungle");
        DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("Cinderella");
        swap(jungleDVD, cinderellaDVD);
        System.out.println(jungleDVD.getTitle());

    }
    //swap function to swap 2 objects
    public static void swap(DigitalVideoDisc x1, DigitalVideoDisc x2){
        DigitalVideoDisc temp = new DigitalVideoDisc("Temp");
        temp.equals(x1);
        x1.equals(x2);
        x2.equals(temp);
    }
}
